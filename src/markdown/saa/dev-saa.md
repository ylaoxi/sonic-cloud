# sonic-android-apk
本文为Sonic安卓助手sonic-android-apk的开发文档。 👉[Github地址](https://github.com/SonicCloudOrg/sonic-android-apk)

<a href="#">  
<img src="https://img.shields.io/github/stars/SonicCloudOrg/sonic-android-apk?style=social">
<img style="margin-left:10px" src="https://img.shields.io/github/forks/SonicCloudOrg/sonic-android-apk?style=social">
</a>

## 本仓库贡献者

<a href="https://github.com/SonicCloudOrg/sonic-android-apk/graphs/contributors">
  <img src="https://contrib.rocks/image?repo=SonicCloudOrg/sonic-android-apk" />
</a>

## 开发环境搭建

建设中...

## 本文贡献者
<div class="cont">
<a href="https://gitee.com/ZhouYixun" target="_blank">
<img src="https://portrait.gitee.com/uploads/avatars/user/2698/8096045_ZhouYixun_1645499109.png!avatar100" width="50"/>
<span>ZhouYixun</span>
</a>
</div>


&nbsp;
&nbsp;
***
不够详细？[点此](https://gitee.com/sonic-cloud/sonic-cloud/edit/master/src/markdown/saa/dev-saa.md) 发起贡献改善此页